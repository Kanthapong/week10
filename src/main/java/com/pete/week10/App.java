package com.pete.week10;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Rectangle rec = new Rectangle(5, 2);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle circle = new Circle(2);
        System.out.println(circle);
        System.out.printf("%s area: %.3f \n",circle.getName(),circle.calArea());
        System.out.printf("%s perimeter: %.3f \n",circle.getName(),circle.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2);
        System.out.printf("%s area: %.3f \n",circle.getName(),circle2.calArea());
        System.out.printf("%s perimeter: %.3f \n",circle.getName(),circle2.calPerimeter());

        Triangle triangle = new Triangle(2, 2, 2);
        System.out.println(triangle);
        System.out.println(triangle.calArea());
        System.out.println(triangle.calPerimeter());

        Triangle triangle2 = new Triangle(3, 5, 6);
        System.out.println(triangle2);
        System.out.println(triangle2.calArea());
        System.out.println(triangle2.calPerimeter());
    }
}
